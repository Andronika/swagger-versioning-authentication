﻿using Authentication.Data;
using Authentication.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Authentication.Controllers
{

    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    public class StudentController : ControllerBase
    {

        /// <summary>
        /// Get students
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public ActionResult<IList<Student>> GetStudents()
        {
            return StudentsInitializer.students;
        }

        /// <summary>
        ///     Create student
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public IActionResult Create(Student student)
        {
            if (student == null)
                return BadRequest();

            StudentsInitializer.students.Add(student);
            return Created($"{HttpContext.Request.Path.Value}/{student.Fistname}", student);
        }

        /// <summary>
        /// Removes student from list
        /// </summary>
        /// <param name="studentLastName"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize]
        [ApiVersion("2.0")]
        public IActionResult Remove(Guid id)
        {
            var student = StudentsInitializer.students.FirstOrDefault(s => s.Id == id);
            if (student == null)
                return NotFound();

            StudentsInitializer.students.Remove(student);
            return NoContent();
        }
    }
}