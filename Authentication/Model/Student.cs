﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Authentication.Model
{
    public class Student
    {
        [Required]
        public Guid Id{ get; set; }
        public string Fistname { get; set; }
        public string Lastname { get; set; }
        public List<string> Grades { get; set; }

    }
}
