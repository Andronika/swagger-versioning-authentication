﻿using Microsoft.AspNetCore.Authentication;

namespace Authentication
{
    public class ApiKeyAuthenticationOptions : AuthenticationSchemeOptions
    {
        public const string DefaultScheme = "Api Key";
        public string Scheme => DefaultScheme;
        public string AuthenticationType => DefaultScheme;
    }
}
