﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;

namespace Authentication.Extension
{
    public class ByteDocumentFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Tags.Select(p => p.Name).FirstOrDefault() == "Student")
            {
                foreach (var param in operation.Parameters)
                {
                    if (param.Name == "lastName")
                    {
                        param.Schema.Pattern = "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$";
                    }
                }
            }

        }
    }
}
