﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;

namespace Authentication
{
    internal class ReplaceVersion : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            if (swaggerDoc == null)
            {
                throw new ArgumentNullException(nameof(swaggerDoc));
            }

            var replecements = new OpenApiPaths();

            foreach (var (key,value) in swaggerDoc.Paths)
            {
                replecements.Add(key.Replace("{version}", swaggerDoc.Info.Version, StringComparison.InvariantCulture), value);
            }
            swaggerDoc.Paths = replecements;
        }
    }
}