﻿using Authentication.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Authentication.Data
{
    public class StudentsInitializer
    {
        public static List<Student> students;
        public List<Student> Seed()
        {
            students = Enumerable.Range(1, 5).Select(index => new Student
            {
               Id = Guid.NewGuid(),
               Fistname = "Fistname" + index,
               Grades = new List<string> {"1", "2", "3"}
            }).ToList();

            return students;
        }
    }
}
