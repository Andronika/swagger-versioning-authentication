# swagger-versioning-authentication

Project (.NET Core 3.1) contains swagger documentation of REST endpoints.  Concentrated on Swashbuckle usage.
- Implemented versioning
- Implemented authentication
- Added description of endpoints